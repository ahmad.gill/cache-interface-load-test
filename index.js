
const Koa = require('koa');
const Router = require('koa-router');
const app = new Koa();
const router = new Router();
const winston = require('winston');
const CacheInterfaceBuilder = require('./cacheInterface/Interface/CacheInterfaceBuilder');
const bodyParser = require('koa-bodyparser');


const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' })
  ]
});
var counter =0;
const cacheInterface = CacheInterfaceBuilder.getCacheInterface(logger);
// Define a middleware to handle incoming requests
app.use(bodyParser());
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const end = Date.now();
  const responseTime = end - start;
  ctx.set('X-Response-Time', `${responseTime}ms`);
  ctx.body = `X-Response-Time - ${responseTime}ms`;
  console.log(`X-Response-Time - ${responseTime}ms`)
});

async function processRequest (request){
    if(request.op  && request.op == "GET"){
       return  await cacheInterface.get(request.key)
    }else  if(request.op  && request.op ==  "SET"){
       return  await cacheInterface.set(request.key, request.data)
    }else{
        console.log("Going for get and set")
        counter = counter + 1
        return await cacheInterface.getOrset(request.key, ()=>{
            return {
                counter : counter ,
                load : 'test'
            }
        })
    }
}

router.get('/GETORSET', async (ctx) => {
  
  
  processRequest(    {
    "key" : "TEST11",
    "data" : {
        "dada" : 1213
    }
})

  ctx.body = 'Request processed successfully!';
});

router.get('/post1', async (ctx) => {
   
    ctx.body = 'Request processed successfully!';
  });

router.get('/GET', async (ctx) => {
    ctx.body  = processRequest(    {
        "key" : "TEST12",
        "op" : "GET"
       
    })
    
});


router.get('/SET', async (ctx) => {
    processRequest(    {
        "key" : "TEST12",
        "op" : "SET",
        "data" : {
            "dada" : 1213
        }
       
    })
    ctx.body = 'Request processed successfully!';
});

app.use(router.routes());
// Start the server and listen for incoming requests
app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
