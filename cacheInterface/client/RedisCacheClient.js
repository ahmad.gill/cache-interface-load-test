const Cache = require('./AbstractClient')
const Redis = require('ioredis');

class RedisCacheClient extends Cache {
    constructor(LOGGER) {
      super(LOGGER);
      if(!process.env.CACHE_REDIS_URL || !process.env.CACHE_REDIS_PORT){
        throw new Error("CACHE_REDIS_URL or CACHE_REDIS_PORT env variable is missing");
      }
      try{
        this.LOGGER.info(`RedisCacheClient - creating connection to Redis Server ${process.env.CACHE_REDIS_URL } : ${process.env.CACHE_REDIS_PORT}`)
        this.cache =   new Redis({
          host: process.env.CACHE_REDIS_URL || localhost,
          port: process.env.CACHE_REDIS_PORT || 6379,
        });
        this.LOGGER.info(`RedisCacheClient  Setup`)
      }catch(err){
        this.LOGGER.info(`RedisCacheClient - ${err}`)
    }
    }
  
    async get(key) {
      try {
        this.LOGGER.info(`RedisCacheClient.get - getting ${key}`)
        return JSON.parse(await this.cache.get(key));
      }catch(err){
        this.LOGGER.error(`RedisCacheClient.get - ${err}`)
      }
    }
  
    async set(key, value, options={}) {
      let parsedValue = JSON.stringify(value);
      try {
        this.LOGGER.info(`RedisCacheClient.set - setting ${key} with ${parsedValue} - Options ${JSON.stringify(options)}`)
        if(options.expireTimeInSec){
          return await this.cache.set(key, parsedValue, 'EX', options.expireTimeInSec);
        }else{
          
          return await this.cache.set(key, parsedValue);
        }
      }catch(err){
        this.LOGGER.error(`RedisCacheClient.set - ${err}`)
      }
      
    }
  
   async delete(key) {
      try {
        this.LOGGER.info(`RedisCacheClient.delete - deleting ${key}`)
        const count = await this.cache.del(key);
        this.LOGGER.info(`RedisCacheClient.delete - deleting ${key} - ${count} keys deleted`)
        return;
      } catch (err) {
        this.LOGGER.error(`RedisCacheClient.delete - ${err}`)
      }
    }
  
    async clear() {
      try {
        this.LOGGER.info(`RedisCacheClient.clear`)
        await this.cache.flushdb();
        this.LOGGER.info(`RedisCacheClient.Cache Cleared`)
      } catch (err) {
        this.LOGGER.error(`RedisCacheClient.clear - ${err}`)
      }
    }
  }

  module.exports = RedisCacheClient;