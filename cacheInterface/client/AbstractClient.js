class AbstractClient{
    constructor(LOGGER) {
        this.LOGGER = LOGGER
        if (new.target === AbstractClient) {
          throw new TypeError("Cannot construct AbstractClass instances directly");
        }
      }

      async get(key) {
        throw new Error("Method 'get' must be implemented");
      }
    
      async set(key, value, options={}) {
        throw new Error("Method 'set' must be implemented");
      }
    
      async delete(key) {
        throw new Error("Method 'delete' must be implemented");
      }
    
      async clear() {
        throw new Error("Method 'clear' must be implemented");
      }
}

module.exports = AbstractClient;