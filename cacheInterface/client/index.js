const LocalCacheClient = require('./LocalCacheClient');
const RedisCacheClient = require('./RedisCacheClient');

module.exports = {
    LocalCacheClient,
    RedisCacheClient
};