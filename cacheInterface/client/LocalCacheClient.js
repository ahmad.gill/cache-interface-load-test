const Cache = require('./AbstractClient')
class LocalCacheClient extends Cache {
    constructor(LOGGER) {
      super(LOGGER);
      this.cache = new Map();
    }
  
    async get(key) {
      this.LOGGER.info(`LocalCacheClient.get - getting ${key}`)
      return this.cache.get(key);
    }
  
    async set(key, value, options={}) {
      this.LOGGER.info(`LocalCacheClient.set - setting ${key} with ${value} - Options ${JSON.stringify(options)}`)
      this.cache.set(key, value);
    }
  
    async delete(key) {
      this.LOGGER.info(`LocalCacheClient.delete - deleting ${key}`)
      this.cache.delete(key);
    }
  
    async clear() {
      this.LOGGER.info(`LocalCacheClient.clear`)
      this.cache.clear();
    }
  }

  module.exports = LocalCacheClient;