const { 
    LocalCacheClient,
    RedisCacheClient
} = require('../client')
const CacheInterface = require('./CacheInterface.js')

class CacheInterfaceBuilder {
    static cacheInterface = null;
    static build(LOGGER){
        if(process.env.CACHE_TYPE == 'AWS-ELASTIC-CACHE'){
            LOGGER.info(`Cache initialized with AWS REDIS client`)
            return new CacheInterface(
                new RedisCacheClient(LOGGER),
                LOGGER
            )
        }else{
            LOGGER.info(`Cache initialized with Local Cache`)
            return new CacheInterface(
                new LocalCacheClient(LOGGER),
                LOGGER
            )
        }

    }

    static getCacheInterface(LOGGER){
        if(CacheInterfaceBuilder.cacheInterface){
            return CacheInterfaceBuilder.cacheInterface;
        }else{
            CacheInterfaceBuilder.cacheInterface = CacheInterfaceBuilder.build(LOGGER);
            return CacheInterfaceBuilder.cacheInterface;
        }

    }

}

module.exports = CacheInterfaceBuilder