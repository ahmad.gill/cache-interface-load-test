class CacheInterface{
    constructor(client, LOGGER) {
        this.client  = client;
        this.LOGGER = LOGGER;
    }

    async get(key) {
        this.LOGGER.info(`CacheInterface.get - getting ${key}`)
        return await this.client.get(key);
      }


      async getOrset(key, dataFunction, options={}) {
        this.LOGGER.info(`LocalCacheClient.getOrset - getting ${key} - Options ${JSON.stringify(options)}`)
        let value = await this.client.get(key);
        if(value){
          return value;
        }else{
          value = await dataFunction()
          this.LOGGER.info(`LocalCacheClient.getOrset - Setting ${key} with ${JSON.stringify(value)}- Options ${JSON.stringify(options)}`)
          await this.client.set(key, value, options);
          return value;
        }
      }
    
    async set(key, value, options={}) {
        this.LOGGER.info(`CacheInterface.set - settting ${key} with ${JSON.stringify(value)} - Options ${JSON.stringify(options)}`)
        await this.client.set(key, value, options );
    }
    
    async delete(key) {
        this.LOGGER.info(`CacheInterface.delete - deleting ${key} `)
        await this.client.delete(key);
    }
    
    async clear() {
        this.LOGGER.info(`CacheInterface.clear - clearing Cache`)
        await this.client.clear();
     }

}

module.exports = CacheInterface;
